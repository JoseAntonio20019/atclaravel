<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routes', function (Blueprint $table) {
            
            //Datos de las rutas 

            $table->id()->unique();
            $table->integer('user_id');
            $table->integer('travel_type_id');
            $table->string('home');
            $table->string('destination');
            $table->integer('places');
            $table->integer('costs');
            $table->string('photo')->default('rout.jpg');
            $table->timestamps();

            //FK
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routes');
    }
};
