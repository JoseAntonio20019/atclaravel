<?php

namespace Database\Seeders;

use App\Models\Routes;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Routes::factory()->count(20)->create();
        User::factory()->count(5)->create();
    }
}
