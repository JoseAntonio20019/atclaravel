<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Routes>
 */
class RoutesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            
            'user_id'=>\App\Models\User::all()->random()->id,
            'travel_type_id'=>random_int(1,2),
            'home'=> $this->faker->sentence,
            'destination'=>$this->faker->sentence,
            'places'=>random_int(1,5),
            'costs'=>random_int(10,100),
            'photo'=>'rout.jpg'
        ];
    }
}
