<?php

namespace App\Myservice;

class Modal{


    //Facade que genera una ventana modal y pide como datos el tipo, el titulo, el texto y la ruta a la que va

    public static function check($type,$title,$text=null,$rout){

        return view('alerts.modal', compact('type','title','text','rout'));

    }


}