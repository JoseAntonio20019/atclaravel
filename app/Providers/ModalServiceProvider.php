<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Myservice\Modal;

class ModalServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('modal',Modal::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
