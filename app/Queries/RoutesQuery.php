<?php

namespace App\Queries;

use App\Models\Routes;
use App\Models\TravelType;


class RoutesQuery
{

    //Función para obtener el tipo de trayecto 

    public static function getByTravelType(TravelType $traveltype)
    {
        return $traveltype->routes()->where('travel_type_id',$traveltype->id)->latest('updated_at')->paginate(7);
    }

    //Función para obtener todas las rutas 
    
    public static function getAll()
    {
        return Routes::latest('updated_at')->paginate(7);
    }
}
