<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Routes;

class RoutesUser extends Model
{
    use HasFactory;

    //Datos requeridos

    protected $fillable=['user_id','routes_id'];


    //Función para obtener los datos de los usuarios de la tabla usuarios

    public function users(){

        return $this->belongsTo(User::class,'user_id');

    }


}
