<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Routes extends Model
{
    use HasFactory;

    //Datos fillable de la tabla

    protected $fillable= ['user_id','travel_type_id','home','destination','places','costs'];



    //Función para obtener los datos del user_id en la tabla usuarios
    public function creator(){

        return $this->belongsTo(User::class,'user_id');
    }

    //Función para obtener los tipos de trayecto de la tabla traveltype
    public function traveltype()
    {

        return $this->belongsTo(TravelType::class,'travel_type_id');

      }

      //Función para obtener datos de los usuarios de la tabla routes_users
      public function users(){


        return $this->belongsToMany(User::class,'routes_users');
      }



}


