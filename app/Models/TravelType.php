<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TravelType extends Model
{
    use HasFactory;

    //Datos requeridos 

    protected $fillable= ['name','slug','color'];


    //Función para obtener el slug 

    public function getRouteKeyName(){

        return 'slug';
      }


      //Función para mostrar que el traveltype está en muchas rutas
      public function routes(){


        return $this->hasMany(Routes::class);

      }
}
    

