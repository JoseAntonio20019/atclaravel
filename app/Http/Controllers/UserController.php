<?php

namespace App\Http\Controllers;

use App\Http\Requests\UploadAvatarForm;
use App\Models\Routes;
use App\Models\RoutesUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modal;

class UserController extends Controller
{
    

    public function index(){


        $user=Auth::user();


        return view('users.profile',compact('user'));

    }

    public function edit(){


        $user=Auth::user();

        return view('users.edit-user',compact('user'));
    }

    public function update(Request $request){

    
        $user=Auth::user();

        $user->name=$request->name;
        $user->email=$request->email;
        $user->password=$request->password;
        $user->save();
        
        return back()->with('success','User updated successfully!');


    }

    public function viewRoutes(){


        $user=Auth::user();

        $routes= Routes::where('user_id','like',$user->id)->get();
        return view('users.view-routes',compact('routes'));



    }

    public function delete(){

        $user=Auth::user();
        $user->delete();

        return redirect('/login');


        
    }

    public function indexAvatar(){


        return view('users.add-avatar');

    }

    //Añadir Avatar 

    public function addAvatar(UploadAvatarForm $request){
        

                    //Ruta donde se va a guardar 
                    $request->file('avatar')->store('public');
                    $user=Auth::user(); 

                    //Carpeta donde se almacenará el dato
                    $user->avatar= asset('storage/'.$request->file('avatar')->hashName());

                    //Actualizar usuario
                    $user->save();

                    //Devuelta a la ruta
    
                    return redirect('/routes')->with('success','Avatar uploaded successfully');
        
                }
}
