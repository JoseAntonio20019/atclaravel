<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoutesForm;
use App\Models\Routes;
use App\Models\RoutesUser;
use App\Models\TravelType;
use App\Queries\RoutesQuery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoutesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     //Función que muestra todas las rutas, hace una busqueda personalizada por tipo de trayecto y comprueba si se ha introducido algo en el buscador y muestra los datos que coincidan con los parámetros
    public function index(TravelType $traveltype = null)
    {

        //Checkeo de si no se ha seleccionado tipo de trayecto
        if ($traveltype != null) {

            $routes = RoutesQuery::getByTravelType($traveltype);

        } else if (request()->exists('search')) {

            //B
            $search = trim(request()->get('search'));

            //Split para hacer búsqueda con espacios

            $searchValues = preg_split('/\s+/', $search, -1, PREG_SPLIT_NO_EMPTY);

            $routes = Routes::where(function ($q) use ($searchValues) {

                foreach ($searchValues as $value) {


                    $q->where('home', 'like', '%' . $value . '%')->orWhere('destination', 'like', '%' . $value . '%');
                }
            })->paginate(7);
        } else {


            //Obtener todas las queries por el archivo Queries
            $routes = RoutesQuery::getAll();
        }


        //Devuelta a la vista

        return view('routes/index', compact('routes', 'traveltype'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     //Creación de ruta
    public function store(RoutesForm $request)
    {


        //Checkeo de si el usuario está verificado 

        $approved = Auth::user()->isVerified() ? true : false;

        //Merge de los datos del usuarios

        $request->merge(['user_id' => Auth::id(), 'approved' => $approved]);

        //Creación de la ruta

        Routes::create($request->all());

        //Devuelta a la vista 

        return redirect('/routes')->with('success', 'Route created successfully');
    }

    public function viewDetails(Request $request)
    {

        //Búsqueda de la ruta por id

        $rout = Routes::find($request->id);

        //Búsqueda de los usuarios de la ruta por id 

        $users = RoutesUser::where('routes_id', 'like', $request->id)->get();

        //Devuelta a la vista

        return view('routes.view-rout', compact('rout', 'users'));
    }


    //Editar Ruta

    public function edit(Request $request)
    {

        //Obtener ruta por id 
        $rout = Routes::where('id', 'like', $request->id)->get();

        //Devuelta a la ruta
        return view('routes.edit-route', compact('rout'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Routes  $routes
     * @return \Illuminate\Http\Response
     */

     //Actualizar Ruta
    public function update(Request $request)
    {

        //Obtener ruta a actualizar
        $rout = Routes::where('id', 'like', $request->id)->first();

        //Parámetros a actualizar
        $rout->home = $request->home;
        $rout->destination = $request->destination;
        $rout->places = $request->places;
        $rout->costs = $request->costs;
        $rout->save();

        return back()->with('success', 'Route Updated Successfully!');
    }


    //Borrar Ruta
    public function delete(Request $request)
    {

        //Obtener ruta por id

        $rout = Routes::where('id', 'like', $request->id);

        //Borrar Ruta
        $rout->delete();


        //Devuelta a la ruta
        return back()->with('success', 'Route deleted succesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Routes  $routes
     * @return \Illuminate\Http\Response
     */
    public function show(Routes $routes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Routes  $routes
     * @return \Illuminate\Http\Response
     */




    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Routes  $routes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Routes $routes)
    {
    }
}
