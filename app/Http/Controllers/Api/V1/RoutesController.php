<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Routes;
use App\Models\TravelType;
use App\Queries\RoutesQuery;
use Illuminate\Http\Request;

class RoutesController extends Controller
{



    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(TravelType $traveltype = null)
    {

        if ($traveltype != null) {

            $routes = RoutesQuery::getByTravelType($traveltype);
        } else if (request()->exists('search')) {

            $search = trim(request()->get('search'));
            $searchValues = preg_split('/\s+/', $search, -1, PREG_SPLIT_NO_EMPTY);

            $routes = Routes::where(function ($q) use ($searchValues) {

                foreach ($searchValues as $value) {


                    $q->where('home', 'like', '%' . $value . '%')->orWhere('destination', 'like', '%' . $value . '%');
                }
            })->paginate(7);
        } else {


            $routes = RoutesQuery::getAll();
        }


        
        return response()->json(['Routes'=>$routes],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Routes  $routes
     * @return \Illuminate\Http\Response
     */
    public function show(Routes $routes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Routes  $routes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Routes $routes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Routes  $routes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Routes $routes)
    {
        //
    }
}
