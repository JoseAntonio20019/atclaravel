<?php

namespace App\Http\Controllers;

use App\Models\Routes;
use App\Models\RoutesUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoutesUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


     //Añadir usuario a la ruta

    public function store(Routes $rout){
  

        //Buscar el usuario
        $places=Routes::find($rout->id);
        
        //Hacer un merge del id del usuario actual con el de la tala
        $rout= RoutesUser::firstOrNew(['user_id' => Auth::id(),'routes_id' => $rout->id]);
        
       
        //Si pulsa otra vez se le elimina de la vista y se devuelve la plaza que ocupaba
        if ($rout->id) {

            $places->increment('places');
            $rout->delete();
    
        } else {

            //Se añade al usuario en la tabla y ocupa una plaza
            
            $places->decrement('places');
            $rout->save();
        
            };
        
        
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RoutesUser  $routesUser
     * @return \Illuminate\Http\Response
     */
    public function show(RoutesUser $routesUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RoutesUser  $routesUser
     * @return \Illuminate\Http\Response
     */
    public function edit(RoutesUser $routesUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RoutesUser  $routesUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RoutesUser $routesUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RoutesUser  $routesUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(RoutesUser $routesUser)
    {
        //
    }
}
