<?php

namespace App\Http\Controllers;

use App\Models\TravelType;
use Illuminate\Http\Request;

class AddRoutesController extends Controller
{
    

    //Función para mostrar la vista de creación de vistas
    
    public function index(){



        $traveltypes=TravelType::orderBy('name','asc')->get();
        return view('routes.create_route',compact('traveltypes'));


    }

}
