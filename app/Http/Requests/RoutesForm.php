<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoutesForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

     //Requerimientos de la ruta 
     
    public function rules()
    {
        return [
            'travel_type_id' => 'required|exists:travel_types,id',
            'home' => 'required',
            'destination' => 'required',
            'places' => 'required',
            'costs' => 'required',
        ];
    }
}
