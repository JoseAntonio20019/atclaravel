<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        //Obtener los dos dígitos para obtener el lenguaje 

        $locale=substr($request->server('HTTP_ACCEPT_LANGUAGE'),0,2);
      
       
        //Check para comprobar el lenguaje

        if($locale != 'en' && $locale != 'es'){

            App::setLocale('en');

        }else{
            App::setLocale($locale);
        }
        
        return $next($request);
    }
}