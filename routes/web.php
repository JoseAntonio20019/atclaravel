<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;





/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'language'], function(){
Auth::routes(['verify'=>'true']);
Route::group(['middleware'=>'verified'], function(){

    //Rutas Rutas
    Route::get('routes',[App\Http\Controllers\RoutesController::class,'index']);
    Route::post('addroutes',[App\Http\Controllers\RoutesController::class,'store']);
    Route::get('addroutes',[App\Http\Controllers\AddRoutesController::class,'index']);
    Route::get('routes/{traveltype}', [App\Http\Controllers\RoutesController::class, 'index']);
    Route::post('joins/{rout}',[App\Http\Controllers\RoutesUserController::class, 'store']);
    Route::get('view/{id}', [App\Http\Controllers\RoutesController::class,'view']);
    Route::get('routDetails/{id}', [App\Http\Controllers\RoutesController::class, 'viewDetails']);
    Route::get('editRout/{id}', [App\Http\Controllers\RoutesController::class, 'edit']);
    Route::post('update/{id}', [App\Http\Controllers\RoutesController::class, 'update']);
    Route::post('deleteRout/{id}', [App\Http\Controllers\RoutesController::class, 'delete'])->name('deleteRout');

    //Rutas Usuarios
    Route::get('profile/{id}',[App\Http\Controllers\UserController::class,'index']);
    Route::get('edituser/{id}', [App\Http\Controllers\UserController::class,'edit']);
    Route::post('edituser/{id}', [App\Http\Controllers\UserController::class, 'update']);
    Route::post('deleteuser/{id}', [App\Http\Controllers\UserController::class,'delete'])->name('deleteUser');
    Route::get('addAvatar', [App\Http\Controllers\UserController::class,'indexAvatar']);
    Route::post('addAvatar', [App\Http\Controllers\UserController::class, 'addAvatar']);
    Route::get('viewRoutes', [App\Http\Controllers\UserController::class, 'viewRoutes']);

});



Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
});
