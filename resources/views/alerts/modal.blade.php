    <!-- Modal -->
    <div class="modal fade" id="{{$type}}Modal" tabindex="-1" aria-labelledby="{{$type}}ModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="{{$type}}ModalLabel">{{$title}}</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            {{$text}}
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{__('Cancel')}}</button>
                            <form method="POST" action="{{$rout}}">
                              @csrf
                            <button type="sumbit" class="btn btn-danger">{{__('Delete')}}</button>
                            </form>
                        </div>
                        </div>
                    </div>
                </div> 