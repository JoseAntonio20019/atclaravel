@include('layouts.app')
@section('content')
<div class="container w-75">
            <div class="card">
                <div class="card-header">
                    <h3>{{__('Create a Rout')}}</h3>
                </div>
                <div class="card-body">
                    <form method="POST" enctype="multipart/form-data" action="/addroutes">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="Channel">{{__('Travel Type')}}:</label>
                            <select class="form-control @error('travel_type_id') is-invalid @enderror" name="travel_type_id">
                            <option selected disabled>{{__('Pick a Travel Type')}}...</option>
                            @foreach ($traveltypes as $travel_type)
                            <option value="{{ $travel_type->id }}">
                            {{ $travel_type->name }}
                            </option>
                            @endforeach
                            </select>
                            @error('travel_type_id')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="plazas">{{__('Home')}}:</label>

                            <input type="text" class="form-control @error('home') is-invalid @enderror" id="home" name="home" value="{{old('home')}}" placeholder="Write from where start the route">

                        </div>
                        @error('home')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror

                        <div class="form-group">
                            <label for="destination">{{__('Destination')}}:</label>

                            <input type="text" class="form-control @error('destination') is-invalid @enderror" id="destination" name="destination" value="{{old('destination')}}" placeholder="Write the destination">

                        </div>
                        @error('destination')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror

                        <div class="form-group">
                            <label for="places">{{__('Places')}}:</label>

                            <input type="number" class="form-control @error('places') is-invalid @enderror" id="places" name="places" value="{{old('places')}}" placeholder="3">

                        </div>
                        @error('places')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror

                    
                        <div class="form-group">
                            <label for="title">{{__('Costs/Person')}}:</label>

                            <input type="number" class="form-control @error('costs') is-invalid @enderror" id="costs" name="costs" value="{{old('costs')}}" placeholder="50€">

                        </div>

                        @error('costs')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror

                        <!-- Da problemas, comentado -->
                        <!--<div class="form-group">
                            <label for="title">{{__('Rout Photo')}} URL:({{__('Optional')}})</label>

                            <input type="file" class="form-control" id="photo" value="{{old('photo')}}" name="photo">

                        </div>-->

                        <div class="form-group card-footer">
                            <button class="btn btn-primary">{{__('Create Route')}}</button>
                        </div>
                    </form>
                </div>
            </div>  
</div>

