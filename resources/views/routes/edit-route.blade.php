@include('layouts.app')


<div class="container w-75">



    <h2 class="ms-3">{{__('Edit Route')}}</h2>
    <div class="card mt-3 ms-3 p-3">


        <form method="POST" enctype="multipart/form-data" action="/update/{{$rout[0]->id}}">
            @csrf

            <div class="form-group mb-3 ms-2">
                <label for="home">{{__('Home')}}:</label><input type="text" name="home" value="{{$rout[0]->home}}">
            </div>


            <div class="form-group mb-3 ms-2">
                <label for="destination">{{__('Destination')}}:<input type="text" name="destination" value="{{$rout[0]->destination}}"></label>
            </div>
            <div class="form-group mb-3 ms-2">
                <label for="places">{{__('Places')}}:<input type="number" name="places" value="{{$rout[0]->places}}"></label>
            </div>
            <div class="form-group mb-3 ms-2">
                <label for="home">{{__('Costs/Person')}}:<input type="number" name="costs" value="{{$rout[0]->costs}}"></label>
            </div>
            <button class="btn btn-primary text-center" type="submit">{{__('Edit Rout')}}</button>



        </form>


    </div>




</div>