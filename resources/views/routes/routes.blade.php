<div class="col-md-12">

        @include('flash-message')


        @yield('content')
            <h2><a class="text-black" href="/routes">{{__('Routes')}}</a> 
                @if($traveltype != null)
                - {{$traveltype->slug}}
                @endif
            </h2>

            <a class="btn btn-success ms-auto" href="/addroutes">{{__('Add Rout')}}</a>
            @foreach ($routes as $rout)
            <li class="card d-flex flex-col p-3 ms-3 mt-3">

            <div class="d-flex flex-row">
            <span style="background:{{ $rout->traveltype->color }};">
            <a style="color:black; text-decoration:none" href="routes/{{$rout->traveltype->slug}}"> {{$rout->traveltype->name}}</a> 
            </span>
                <p class="ms-3" target="_blank">
                    {{$rout->home}}->{{$rout->destination}}
                </p>
            </div>
                <small>{{__('Places')}}:{{$rout->places}}</small>
                <small>{{__('Costs/Person')}}:{{$rout->costs}}€</small>
                <small>{{__('Created by')}}: {{$rout->creator->name}} {{$rout->created_at->diffForHumans()}}</small>
                <small>{{__('Email')}}: {{$rout->creator->email}}</small>
                <small class="mb-3">{{__('People in the rout')}}:{{$rout->users()->count()}}</small>
                <img class="mb-3" src="{{$rout->image}}" width="200px" height="200px">
                <form method="POST" action="joins/{{ $rout->id }}">
                    {{ csrf_field() }}
                    <button class="btn {{Auth::user()->id == $rout->creator->id ? 'disabled btn-warning':'active'}} 
                                       {{Auth::check() && Auth::user()->joinedFor($rout) ? 'btn-danger' : 'btn-success' }} 
                                       {{$rout->places==0 && Auth::user()->joinedFor($rout)==false ? 'disabled btn-warning':'active'}}">
                    
                    @if(Auth::user()->joinedFor($rout))
                        {{__('Remove from the rout')}}

                    @elseif($rout->places==0)
                        {{__('Rout Full')}}    
                    @elseif(Auth::user()->id == $rout->creator->id)
                        {{__('You are the creator of the Rout')}}
                    @else
                        {{__('Join')}}
                    @endif    
                    </button>

            </form>
            </li>
            
            @endforeach
        </div>