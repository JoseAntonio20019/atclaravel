@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @include('routes.routes')

        {{$routes->links('pagination::bootstrap-4')}}
    </div>
    

</div>


@stop