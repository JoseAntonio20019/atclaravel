@include('layouts.app')

<div class="container w-75">

<h2>{{__('Rout Details')}}:</h2>

<div class="card mt-3 ms-3 p-3" style="list-style: none;">
    <h5>{{__('Travel')}}: {{$rout->home}}-{{$rout->destination}}</h5>
    <h5>{{__('Places')}}: {{$rout->places}}</h5>
    <h5>{{__('Costs/Person')}}: {{$rout->costs}}€</h5>
    <h5>{{__('Users')}}:</h5>

    @if(count($users)>0)
    @foreach($users as $user)
    <div class="card mt-3 ms-3 p-3">
    <p>{{__('Name')}}: {{$user->users->name}}</p>
    <p>{{__('Email')}}: {{$user->users->email}}</p>
    </div>

    @endforeach
    @else
    <p>No users yet...</p>
    @endif
    

</div>