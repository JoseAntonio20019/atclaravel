<div class="col-md-12">
            <h2><a class="text-black" href="/routes">{{__('Routes')}}</a> 
                @if($traveltype != null)
                - {{$traveltype->slug}}
                @endif
            </h2>

            <a class="btn btn-success ms-auto" href="/addroutes">{{__('Add Rout')}}</a>
            @foreach ($routes as $rout)
            <li class="card d-flex flex-col p-3 ms-3 mt-3">

            <div class="d-flex flex-row">
            <span style="background:{{ $rout->traveltype->color }};">
            <a style="color:black; text-decoration:none" href="routes/{{$rout->traveltype->slug}}"> {{$rout->traveltype->name}}</a> 
            </span>
                <p class="ms-3" target="_blank">
                    {{$rout->home}}->{{$rout->destination}}
                </p>
            </div>
                <small>{{__('Places')}}:{{$rout->places}}</small>
                <small>{{__('Costs/Person')}}:{{$rout->costs}}€</small>
                <small>{{__('Created by')}}: {{$rout->creator->name}} {{$rout->created_at->diffForHumans()}}</small>
                <small>{{__('Email')}}: {{$rout->creator->email}}</small>
                <small class="mb-3">{{__('People in the rout')}}:{{$rout->users()->count()}}</small>
                <img class="mb-3" src="{{$rout->image}}" width="200px" height="200px">
                <form method="POST" action="joins/{{ $rout->id }}">
                    {{ csrf_field() }}
                    <button class="btn btn-success">Apuntarse</button>

            </form>
            </li>
            
            @endforeach
        </div>