@include('layouts.app')

    <div class="container w-75">

    @include('flash-message')

    @yield('content')

        <h2>{{__('Your Routes')}}:</h2>

        @if(count($routes)==0)

        <h2>{{__('You dont have routes created')}}</h2>

        @else

        @foreach($routes as $rout)

        <div class="card mt-3 ms-3 p-3" style="list-style: none;">
            <h5>{{__('Travel')}}: {{$rout->home}}-{{$rout->destination}}</h5>
            <h5>{{__('Places')}}: {{$rout->places}}</h5>
            <h5>{{__('Costs/Person')}}: {{$rout->costs}}</h5>
    
            <div class="d-flex flex-row m-3 justify-content-around">

            <a class="btn btn-primary" href="routDetails/{{$rout->id}}" >{{__('View Details')}}</a>
            <a class="btn btn-warning" href="editRout/{{$rout->id}}">{{__('Edit Rout')}}</a>

                   <!-- Button trigger modal -->
                    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#deleteModal">
                    {{__('Delete Rout')}}
                    </button>

                
        
      </div>
    </div>
  </div>
</div>
                {{Modal::check('delete','Delete User','Are you sure to delete the rout?',Route('deleteRout',$rout->id))}}        
        

            </div>
        

        </div>
        @endforeach
        @endif



    </div>
