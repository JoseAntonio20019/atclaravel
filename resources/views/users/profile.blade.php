@include('layouts.app')

<div class="container w-75">

        @include('flash-message')

        @yield('content')

    <h1 class="text-center mb-5">{{__('Profile')}}</h1>

    <div class="card d-flex flex-col p-3">
        <div class="text-center mb-5"><img class="text-center rounded-circle" width="200px" height="200px" src="{{$user->avatar}}">
        </div>
    
    <h5>{{__('Name')}}: {{$user->name}}</h5>
    <h5>{{__('E-mail')}}: {{$user->email}}</h5>
    <h5>{{__('Verified')}}: @if($user->verified)<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-patch-check" viewBox="0 0 16 16">
  <path fill-rule="evenodd" d="M10.354 6.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7 8.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
  <path d="m10.273 2.513-.921-.944.715-.698.622.637.89-.011a2.89 2.89 0 0 1 2.924 2.924l-.01.89.636.622a2.89 2.89 0 0 1 0 4.134l-.637.622.011.89a2.89 2.89 0 0 1-2.924 2.924l-.89-.01-.622.636a2.89 2.89 0 0 1-4.134 0l-.622-.637-.89.011a2.89 2.89 0 0 1-2.924-2.924l.01-.89-.636-.622a2.89 2.89 0 0 1 0-4.134l.637-.622-.011-.89a2.89 2.89 0 0 1 2.924-2.924l.89.01.622-.636a2.89 2.89 0 0 1 4.134 0l-.715.698a1.89 1.89 0 0 0-2.704 0l-.92.944-1.32-.016a1.89 1.89 0 0 0-1.911 1.912l.016 1.318-.944.921a1.89 1.89 0 0 0 0 2.704l.944.92-.016 1.32a1.89 1.89 0 0 0 1.912 1.911l1.318-.016.921.944a1.89 1.89 0 0 0 2.704 0l.92-.944 1.32.016a1.89 1.89 0 0 0 1.911-1.912l-.016-1.318.944-.921a1.89 1.89 0 0 0 0-2.704l-.944-.92.016-1.32a1.89 1.89 0 0 0-1.912-1.911l-1.318.016z"/>
</svg>@endif</h5>

        <div class="d-flex flex-row m-3">
            <a class="btn btn-success mx-3" href="/addAvatar">{{__('Upload Avatar')}}</a>
            <a class="btn btn-warning mx-3" href="/edituser/{{$user->id}}">{{__('Edit User')}}</a>

            <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#deleteModal">
                    {{__('Delete User')}}
                    </button>



                    {{Modal::check('delete','Delete User','Are you sure to delete the user?',Route('deleteUser',$user->id))}}        
        </div>
    </div>
    




</div>