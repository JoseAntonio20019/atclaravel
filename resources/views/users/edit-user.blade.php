@include('layouts.app')


<div class="container w-75">

    <h3>{{__('Edit User')}}</h3>
    <div class="card">

                <form class="card" method="POST" enctype='multipart/form-data' action="/edituser/{{$user->id}}">
                    @csrf
                    <div class="form-group mb-3 ms-3 mt-3">
                    <label for="name">{{__('Name')}}: <input type="text" name="name" value="{{$user->name}}"></label>
                    </div>
                    <div class="form-group mb-3 ms-3 mt-3">
                    <label for="email">{{__('E-mail')}}: <input type="text" name="email" value="{{$user->email}}"></label>
                    </div>
                    <div class="form-group mb-3 ms-3 mt-3">
                    <label for="password">{{__('Password')}}: <input type="password" name="password" value="{{$user->password}}"></label>
                    </div>
            
                    <div class="form-group text-center mb-5">
                    <button class="btn btn-primary" type="submit" >{{__('Edit User')}}</button>
                    </div>
                </form>


        </div>




</div>