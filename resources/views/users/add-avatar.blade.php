@extends('layouts.app')

@section('content')
<div class="card w-25 m-auto p-3">
<form method="POST" action="/addAvatar" enctype="multipart/form-data">
@csrf

<label for="img" class="col-md-4 col-form-label text-md-end">{{ __('Avatar') }}
    <input class="mb-3" type="file" name="avatar"></label>
    <br>
    <input class="btn btn-primary" type="submit" value="{{__('Upload')}}">

</form>
</div>
@stop